import { observer } from 'mobx-react-lite'
import { TodoStore } from 'observables'
import { TodoList } from 'components'

const ListTodoContainer = () => {
  const { todos, removeTodo } = TodoStore
  return (
    <TodoList list={todos} onClick={removeTodo} />
  )
}

export default observer(ListTodoContainer)
