import { observer } from 'mobx-react-lite'
import { TodoStore } from 'observables'
import { TodoForm } from 'components'

const AddTodoContainer = () => {
  const { addTodo } = TodoStore
  return (
    <TodoForm onSubmit={addTodo} />
  )
}

export default observer(AddTodoContainer)
