import './Layout.css'
import { Container } from 'react-bootstrap'
import Navbar from './Navbar'

const LayoutComponent = ({ children }: any) => {
  return (
    <div className="layout">
      <Navbar />
      <Container>
        {children}
      </Container>
    </div> 
  )
}

export default LayoutComponent
