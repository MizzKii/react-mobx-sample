import './Navbar.css'
import { Link } from 'react-router-dom'
import { Navbar, Nav, Container } from 'react-bootstrap'
import logo from 'assets/logo.svg'

const NavbarComponent = ({ children }: any) => {
  return (
    <>
      <div className="navbar-space" />
      <Navbar fixed="top" bg="light" variant="light">
        <Container>
          <Navbar.Brand>
            <img alt="Logo" src={logo} width="30" height="30" />
            {' '}
            React mobx sample
          </Navbar.Brand>
          {children}
          <Nav className="ml-auto">
            <Link className="nav-link" role="button" to="/">Home</Link>
            <Link className="nav-link" role="button" to="/add">Add</Link>
            <Link className="nav-link" role="button" to="/todos">Todos</Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  )
}

export default NavbarComponent
