import './TodoForm.css'
import { useState } from 'react'
import PropTypes from 'prop-types'
import { Form, Button, Row, Col } from 'react-bootstrap'

const HandleMessage = (onSubmit: Function) => {
  const [value, onChange] = useState('')
  return {
    value,
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => onChange(e.target.value),
    onSubmit: (e: React.ChangeEvent<HTMLFormElement>) => {
      e.preventDefault()
      if (!value) return
      onSubmit(value)
      onChange('')
    }
  }
}

const TodoFormComponent = ({ onSubmit }: any) => {
  const msg = HandleMessage(onSubmit)

  return (
    <Form className="form" onSubmit={msg.onSubmit}>
      <h2>Add your todo item.</h2>
      <br />
      <Form.Group as={Row}>
        <Form.Label column sm={3} htmlFor="todo-message">Message</Form.Label>
        <Col sm={6}>
          <Form.Control required type="text" id="todo-message" value={msg.value} onChange={msg.onChange} />
        </Col>
        <Col sm={3}>
          <Button type="submit" className="submit-button">Add</Button>
        </Col>
      </Form.Group>
    </Form>
  )
}

TodoFormComponent.propTypes = {
  onSubmit: PropTypes.func.isRequired,
}

export default TodoFormComponent
