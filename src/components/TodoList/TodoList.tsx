import { Alert } from 'react-bootstrap'
import PropTypes from 'prop-types'

const colors = [
  'primary',
  'secondary',
  'success',
  'danger',
  'warning',
  'info',
  'light',
  'dark',
]

interface Todo {
  title: String
}

const TodoListComponent = ({ list, onClick }: any) => {
  return (
    <>
      <h2>Todo list</h2>
      <br />
      { !list.length && <h4>Not found items.</h4> }
      {
        list.map((todo: Todo, idx: number) => (
          <Alert key={idx} variant={colors[idx % 8]} onClick={() => onClick(idx)}>
            Message: {todo.title}
          </Alert>
        ))
      }
    </>
  )
}

TodoListComponent.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
  })).isRequired,
  onClick: PropTypes.func,
}

TodoListComponent.defaultProps = {
  onClick: () => {},
}

export default TodoListComponent
