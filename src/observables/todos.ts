import { types } from 'mobx-state-tree'

const todo = types
  .model('Todo', {
    title: types.optional(types.string, ''),
    status: types.optional(types.string, 'INCOMPLATE'),
  })
  .actions(self => ({
    complated: () => self.status = 'COMPLATED',
    incomplate: () => self.status = 'INCOMPLATE',
  }))

const todoStore = types
  .model('TodoStore', {
    todos: types.array(todo)
  })
  .actions(self => ({
    addTodo: (title: string) => self.todos.push({ title }),
    removeTodo: (idx: number) => self.todos.splice(idx, 1),
  }))
  .views(self => ({
    complates: () => self.todos.filter(t => t.status === 'COMPLATED'),
    incomplates: () => self.todos.filter(t => t.status === 'INCOMPLATE'),
  }))

export default todoStore.create()
