import React from 'react'
import { RouteProps } from 'react-router-dom'

const lazyComponent = (page: string) => {
  const Component = React.lazy(() => import(`pages/${page}`))
  return () => <Component />
}

const routes: RouteProps[] = [
  { path: '/', exact: true, component: lazyComponent('Home') },
  { path: '/add', exact: true, component: lazyComponent('Add') },
  { path: '/todos', exact: true, component: lazyComponent('Todos') },
]

export default routes
