import './Home.css'
import logo from 'assets/logo.svg'

const Home = () => {
  return (
    <>
      <img src={logo} className="App-logo" alt="logo" />
      <p>
        Edit <code>src/App.tsx</code> and save to reload.
      </p>
      <a target="_blank" rel="noopener noreferrer" href="https://reactjs.org">
        Learn React
      </a>
    </>
  )
}

export default Home
