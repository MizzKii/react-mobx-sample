import { Suspense } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Layout from 'components/Layout'
import routes from 'routes'

const App = () => {
  return (
    <Suspense fallback={<label>Loading...</label>}>
      <BrowserRouter>
        <Layout>
          <Switch>
            {routes.map((route, index) => (
              // You can render a <Route> in as many places
              // as you want in your app. It will render along
              // with any other <Route>s that also match the URL.
              // So, a sidebar or breadcrumbs or anything else
              // that requires you to render multiple things
              // in multiple places at the same URL is nothing
              // more than multiple <Route>s.
              <Route key={index} {...route} />
            ))}
          </Switch>
        </Layout>
      </BrowserRouter>
    </Suspense>
  )
}

export default App
