FROM node:16-alpine3.12 as builder
WORKDIR /src

COPY package.json .
COPY yarn.lock .
RUN yarn

COPY . .
RUN yarn build

FROM nginx:1.21.1-alpine
WORKDIR /usr/share/nginx/html

COPY --from=builder /src/build/ ./
